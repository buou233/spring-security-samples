package com.security.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 自定义认证处理器
 * 此处理器只会作用于登录请求
 * @author chai
 * @since 2020/11/18
 */
public class CustomAuthenticationProvider extends DaoAuthenticationProvider {

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        // 在此可以加入自定义的逻辑处理 如验证码、手机号码登录等
        // ...
        super.additionalAuthenticationChecks(userDetails, authentication);
    }
}
