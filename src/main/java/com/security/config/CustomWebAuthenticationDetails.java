package com.security.config;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

/**
 * 自定义设置用户详细信息
 * @author chai
 * @since 2020/11/18
 */
public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {

    private boolean isAdmin;
    private String userName;

    /**
     * Records the remote address and will also set the session Id if a session already
     * exists (it won't create one).
     *
     * @param request that the authentication request was received from
     */
    public CustomWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        isAdmin = true;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "CustomWebAuthenticationDetails{" +
                "isAdmin=" + isAdmin +
                ", userName='" + userName + '\'' +
                "} " + super.toString();
    }
}
