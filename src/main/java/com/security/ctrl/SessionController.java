package com.security.ctrl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * spring-session测试接口
 * @author chai
 * @since 2020/11/19
 */
@RestController
public class SessionController {

    @Value("${server.port}")
    private Integer port;

    @GetMapping("set")
    public String set(HttpSession session) {
        session.setAttribute("user", "java-boy");
        return port.toString();
    }

    @GetMapping("get")
    public String get(HttpSession session) {
        Object attribute = session.getAttribute("user");
        return port + " - " + attribute.toString();
    }

}
