package com.security.ctrl;

import com.security.config.CustomWebAuthenticationDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chai
 * @since 2020/11/3
 */
@RestController
public class HelloController {

    @GetMapping("hello")
    public String hello() {
        System.out.println("----------- Hello World! -----------");
        return "Hello World!";
    }

    @GetMapping("admin/hello")
    public String admin() {
        return "Admin Hello World!";
    }

    @GetMapping("user/hello")
    public String user() {
        return "User Hello World!";
    }

    @GetMapping("details")
    public String details() {
        // 获取该用户的详细信息 如ip地址、sessionId等
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        WebAuthenticationDetails details = (WebAuthenticationDetails)authentication.getDetails();
        // 获取自定义的用户信息
        CustomWebAuthenticationDetails details = (CustomWebAuthenticationDetails)authentication.getDetails();
        details.setUserName(authentication.getName());
        return details.toString();
    }

}
